package impl;

import java.util.function.Function;

public class FunctionalAddThreeImpl implements Function<Long, Long> {

    @Override
    public Long apply(Long aLong) {
        return aLong + 3;
    }
}
