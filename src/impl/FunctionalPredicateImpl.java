package impl;

import java.util.function.Predicate;

public class FunctionalPredicateImpl implements Predicate<String> {

    @Override
    public boolean test(String o) {
        return o != null;
    }
}
