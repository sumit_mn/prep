package algo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test {
	
	public static void main(String[] args) {
		//Segregate the input to grid and words
		//Segregate the input to grid and words
        List<String> inputGrid = new ArrayList<>();
        List<String> inputwords = new ArrayList<>();
        String prev = args[0];
        boolean populateGrid = true;
        for(int i=0; i< args.length; i++){
            if(prev.length() != args[i].length()){
                populateGrid = false;
            }
            if(populateGrid){
                inputGrid.add(args[i]);
            }else{
                inputwords.add(args[i]);
            }
            
            prev = args[i];
        }
        
        String str = "DAREDEVILHRAIE";
        System.out.println(str.startsWith("DAREDEVIL"));

        inputwords = inputwords.stream().sorted().collect(Collectors.toList());
        
        //convert input grid to 2d array
        int inputGridSize = inputGrid.size();
        int wordsSize = inputwords.size();
        
        char[][] inputChars = new char[inputGridSize][inputGrid.get(0).length()];
        int counter = 0;
        for(String i: inputGrid){
            inputChars[counter] = i.toCharArray();
            counter++;
        }
        
        //actual logic
        for(int r=0; r< inputChars.length; r++){
            char[] rowStr = inputChars[r];
            for(int c=0; c< rowStr.length; c++){
                
            	List<String> all8DirectionStrs = getAllDirectionSrings(inputChars, r, c, inputGridSize, rowStr.length);
            	System.out.println(all8DirectionStrs);
                
//                for(int w=0; w< wordsSize; w++){
//                    
//                }
            }
        }
	}
	
	/**
	 * 
ABCD
PRAT
KITA
ANDY

	 * @param inputChars
	 * @param currRow
	 * @param currCol
	 * @param rows
	 * @param cols
	 * @return
	 */
	
	private static List<String> getAllDirectionSrings(char[][] inputChars, int currRow, int currCol, int rows, int cols) {
		List<String> strs= new ArrayList<>();
		strs.add(getDir1(inputChars, currRow, currCol, rows, cols));
		strs.add(getDir2(inputChars, currRow, currCol, rows, cols));
		strs.add(getDir3(inputChars, currRow, currCol, rows, cols));
		strs.add(getDir4(inputChars, currRow, currCol, rows, cols));
		strs.add(getDir5(inputChars, currRow, currCol, rows, cols));
		strs.add(getDir6(inputChars, currRow, currCol, rows, cols));
		strs.add(getDir7(inputChars, currRow, currCol, rows, cols));
		
		return strs;
	}
	
	//c++
	private static String getDir1(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
        for(int c=currCol; c<cols; c++){
        	str += String.valueOf(inputChars[currRow][c]);
        }
        return str;
	}
	
	//r++, c++
	private static String getDir2(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
		for(int r=currRow,c=currCol; r<rows && c<cols; c++,r++){
			str += String.valueOf(inputChars[r][c]);
		}
		
        return str;
	}

	//r++
	private static String getDir3(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
        for(int r=currRow; r<rows; r++){
        	str += String.valueOf(inputChars[r][currCol]);
        }
        return str;
	}
	
	//r++, c--
	private static String getDir4(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
		for(int r=currRow,c=currCol; r<rows && c > -1; c--,r++){
			str += String.valueOf(inputChars[r][c]);
		}
		
        return str;
	}
	
	//c--
	private static String getDir5(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
        for(int c=currCol; c > -1; c--){
        	str += String.valueOf(inputChars[currRow][c]);
        }
        return str;
	}
	
	//r--, c--
	private static String getDir6(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
		for(int r=currRow,c=currCol; r > -1 && c > -1; c--,r--){
			str += String.valueOf(inputChars[r][c]);
		}
		
        return str;
	}
	
	//r--
	private static String getDir7(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
		for(int r=currRow; r > -1; r--){
			str += String.valueOf(inputChars[r][currCol]);
		}
		
        return str;
	}
	
	// r--, c++
	private static String getDir8(char[][] inputChars, int currRow, int currCol, int rows, int cols){
		String str = "";
		for(int r=currRow,c=currCol; r > -1 && c<currCol; c++,r--){
			str += String.valueOf(inputChars[r][c]);
		}
		
        return str;
	}
}
