package algo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 
 * 1. This about total chars 2. Ask about case sensitive 3. A common approach in
 * string manipulation problems or in-place modification is to edit the string
 * starting from the end and working backwards.
 * 
 * @author sumeet
 *
 */
public class StringManipulations {

	public static void main(String[] args) {
		char[] chars = {'a',' ', 'b', 'c', ' ', ' '};
		StringManipulations.URLify(chars, 4);
		System.out.println("URLify chars: "+ String.valueOf(chars));
		
		System.out.println("Has Paliandrom combination: "+StringManipulations.plaiandromPermutations("tataoTactcoa"));
		
		System.out.println(strComparison("aabcccccaaa"));

	}

	// SORT a String
	public static String sortString(String inputStr) {
		char[] chars = inputStr.toCharArray();
		Arrays.sort(chars);

		return new String(chars);
	}

	public void uniqueCharsInString(String input) {
		for(int i=0; i < input.length() ; i++) {
			
		}
	}

	private static void URLify(char[] str, int trueLength) {
		int spaceCount = 0, index, i = 0;
		for (i = 0; i < trueLength; i++) {
			if (str[i] == ' ') {
				spaceCount++;
			}
		}

		index = trueLength + spaceCount * 2;

		if (trueLength < str.length) { 
			str[trueLength] = '\0';
		}

		for (i = trueLength - 1; i >= 0; i-- ) {
			if (str[i] == ' ') {
				str[index - 1] = '0';
				str[index - 2] = '2';
				str[index - 3] = '%';
				index = index - 3;
			 } else {
				 str[index - 1] = str[i];
				 index--;
			 }
		}
	}
	
	private static boolean plaiandromPermutations(String pal) {
		char palChars[] = pal.toCharArray();
		Map<String, Integer> palcharCountMap = new HashMap<>();

		for (int i = 0; i < palChars.length; i++) {
			char c = palChars[i];
			String charStr = String.valueOf(c).toLowerCase();

			if(palcharCountMap.containsKey(charStr)){
				palcharCountMap.put(charStr, palcharCountMap.get(charStr) + 1);
			}else {
				palcharCountMap.put(charStr, 1);
			}
		}
		
		if(pal.length() %2 == 0) {
			for (Map.Entry<String, Integer> c : palcharCountMap.entrySet()) {
				if(c.getValue() %2 !=0 ) return false;
			}
		}else {
			boolean foundSingleChar = false;
			for (Map.Entry<String, Integer> c : palcharCountMap.entrySet()) {
				int val = c.getValue(); 
				if(val %2 != 0 ) return false;
				else if(val == 1 && !foundSingleChar) {
					foundSingleChar = true;
				} else if(val == 1 && foundSingleChar) {
					return false;
				}
			}
		}

		return true;
	}

	//the string aabcccccaaa would become a2blc5a3
	private static String strComparison(String strInput) {
		StringBuilder sb = new StringBuilder();
		char[] chars = strInput.toCharArray();
		
		int counter = 1;
		for (int i = 0; i < chars.length; i++) {
			if((i+1) < chars.length) {

				if(chars[i] == chars[i+1]) {
					counter++;
				}else {
					sb.append(chars[i]).append(counter);
					counter = 1;
				}
			}else {
				sb.append(chars[i]).append(counter);
			}
		}

		return sb.toString();
	}

}
