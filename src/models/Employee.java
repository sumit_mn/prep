package models;

public class Employee {
	int id;
	String name;
	double salary;

	public Employee(int id, String name, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	
	public void salaryIncrement(double d) {
		this.salary = this.salary + d;
	}

	public int getId() {
		return this.id;
	}
	
	public double getSalary() {
		return this.salary;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return "Id: "+ this.id +" Name: "+this.name;
	}
}
