package util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import models.Car;

public class GoodCarYard {

    private Set<Car> carsCol = new HashSet<Car>( );

    public void addCar(Car car) {
        // throw exception early
        if (car == null) {
            new IllegalArgumentException("Cannot add null");
        }
        //more checks here to add a valid Car
        carsCol.add(car);
    }

    public void removeCar(Car car) {
        if (car == null) {
           new IllegalArgumentException("Cannot add null");
        }
        //... more checks here before removing a car
        carsCol.remove(car);
    }

    /**
     * This makes sure you have one true copy of cars
     * @return Set<Car>
     */
    public Set<Car> getCars( ) {
        // prevent addition or removal from outside this class
        return Collections.unmodifiableSet(carsCol);
    }
    
    /**
     * =============> Bad approach <============
     * exposes the carsCol to the caller
     * =========================================
     * 
     * @return Set<Car>
     */ 
    public Set<Car> getCarsCollection( ) {
        return carsCol;
    }
}