package interfaces;

public interface FunctionalSum {

	void abstractSum(int x, int y);

	default void defaultFun() { 
        System.out.println("This is default method");
	}

	default void defaultFun2() {
		System.out.println("This is ANOTHER default method");
	}
}
