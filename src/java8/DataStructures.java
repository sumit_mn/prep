package java8;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import models.Employee;
import util.EmployeeComparator;

/**
 * Java 8 has introduced a �forEach� method in the interface java.lang.Iterable that can iterate over the elements in the collection. 
 * �forEach� is a default method defined in the Iterable interface. 
 * It is used by the Collection classes that extend the Iterable interface to iterate elements.
 * 
 * The �forEach� method takes the Functional Interface as a single parameter i.e. you can pass Lambda Expression as an argument.
 * 
 */
public class DataStructures {
	
	static enum Gfg { CODE, LEARN, CONTRIBUTE, QUIZ, MCQ };
	
	static Integer intsArray[] = { 50, 30, 10, 20, 40, 60, 5, 20};

	static Employee[] arrayOfEmps = {
		new Employee(1, "Jeff Bezos", 100000.0), 
		new Employee(2, "Bill Gates", 200000.0), 
		new Employee(3, "Mark Zuckerberg", 300000.0)
	};
	
	static List<Integer> listOfNums = null;
	
	private static Stack<Integer> stackObj = null;

	public static void main(String[] args) {
		
		dataStructures();
		
		immutableDataStructures();
	}
	
	private static void immutableDataStructures() {
		List<Integer> list = Collections.unmodifiableList(listOfNums);
		//list.add(44);

		System.out.println("immutable list: "+list);		
	}
	
	@SuppressWarnings("unused")
	private static void dataStructures() {
		
		// ====================
		//	Array
		// ====================
		//	- Linear Data Structure
		//	- Elements are stored in contiguous memory locations
		//	- Can access elements randomly using index
		// 	- Stores homogeneous elements i.e, similar elements
		//	- Insertion and deletion are quite costly operation in array as you need to shift elements.
		
		// initialize
		int[] priceOfPen= new int[5];
		String[] strArr = {"One","Two","Three"};
		
		// print array
		System.out.println("initialized String Array: " + Arrays.toString(strArr) );
		System.out.println("initialized Integer Array: " + Arrays.toString(intsArray));

		// ====================
		//	List / ArrayList
		// ====================
		// ArrayList implements List
		// Creates a bigger-sized memory on heap memory (for example memory of double size).
		// Copies the current memory elements to the new memory.
		// New item is added now as there is bigger memory available now.
		// Delete the old memory.
		// ArrayList can not be used for primitive types, like int, char, etc. We need a wrapper class for such cases.
		listOfNums = Arrays.asList(intsArray);
		System.out.println("\n==> List");
		// Printing list
		System.out.println("Printing list in java 8 style: listOfNums.forEach(System.out::println)");
		listOfNums.forEach(System.out::println);

		@SuppressWarnings("serial")
		ArrayList<String> list = new ArrayList<String>() {{
		    add("A");
		    add("B");
		    add("C");
		}};

		List<String> emptyList = Collections.emptyList();
		List<String> list1 = new ArrayList<String>(40);
		
		//////////////////////////////////////////
		// LinkedList	
		//////////////////////////////////////////
		// Is a Class
		// Due to the dynamicity and ease of insertions and deletions, they are preferred over the arrays.
		// the elements are not stored in a continuous fashion.
		// This class is an implementation of the LinkedList data structure which is a linear data structure
		System.out.println("\n==> LinkedList");
		LinkedList<Integer> ll = new LinkedList<Integer>();
		ll.addAll(listOfNums);
		
		System.out.println("Printing LinkedList: "+ll);
		System.out.print("Printing reversed LinkedList:");
		while(!ll.isEmpty()) {
			System.out.print(" "+ll.removeLast());
		}

		//////////////////////////////////////////
		// Stack
		//////////////////////////////////////////
		// Its a class
		// public class Stack<E> extends Vector<E>

		System.out.println("\n\n==> Stack");
		// Print Stack
		stackObj = new Stack<Integer>();
		for(Integer intItem: listOfNums) {
			stackObj.push(intItem);
		}
		
		System.out.println("Top of stack: "+ stackObj.peek());
		System.out.print("Printing items in stack:");
		while( !stackObj.isEmpty() ){
		    System.out.print(" "+stackObj.pop());
		}

		System.out.println();
		//////////////////////////////////////////
		// Deque
		//////////////////////////////////////////
		// public interface Deque extends Queue
		// - Is short for double-ended queue. A deque is a list that supports insertion and removal at both ends. 
		// 	 Thus, a deque can be used as a queue or as a stack
		// - Accessing an element in ArrayDeque is always faster compared to LinkedList with O(1) for accessing elements. 
		// 	 In the Linked list, it will take O(N) to find the last element.
		// - ArrayDeque is memory efficient since you don't have to keep track of the next node unlike in Linked List.
		// - ArrayDeque is a Resizable-array implementation of the Deque interface. Array deques have no capacity restrictions; 
		//	 they grow as necessary to support usage. 
		// - element() - This method is similar to peek(). It throws NoSuchElementException when the queue is empty.

		System.out.println("\n==> Deque");

		// Deque as Queue - FIFO
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for(Integer intItem: listOfNums) {
			// Adds given element to the tail without violating capacity restrictions. Returns true on success and false on failure.
			deque.offer(intItem);		// same as deque.addLast(intItem);
			
		}

		System.out.println("Printing deque as a Queue:: ");
		for(Integer intItem: deque) {
			// poll() - Deletes and returns the head of the deque. Returns null if the deque is empty.
            System.out.print(" " + deque.poll());
		}

		System.out.println();

		// Deque as Stack - LIFO
		deque = new ArrayDeque<Integer>();
		for(Integer intItem: listOfNums) {
			// push() - Adds given element on to the stack represented by queue.
			deque.push(intItem);
		}

		System.out.println("Printing deque as a Stack:: ");
		for(Integer intItem: deque) {
			// poll() - Deletes and returns the head of the deque. Returns null if the deque is empty.
            System.out.print(" " + deque.poll());
		}

		//////////////////////////////////////////
		// PriorityQueue
		//////////////////////////////////////////
		System.out.println("\n\n==> PriorityQueue");
		// The elements of the priority queue are ordered according to the natural ordering, or by a Comparator provided at queue construction time, depending on which constructor is used.
		// PriorityQueue doesn�t permit null.
		// We can�t create PriorityQueue of Objects that are non-comparable
		// PriorityQueue are unbound queues.
		// It provides O(log(n)) time for add and poll methods.
		
		// PriorityQueue(int initialCapacity, Comparator<E> comparator)
		// PriorityQueue<E> pq = new PriorityQueue<E>(SortedSet<E> c);
		PriorityQueue<Integer> pq = new PriorityQueue<>();
		pq.addAll(listOfNums);

		System.out.println("Printing Priority Queue: ");
		while (!pq.isEmpty()) {
            System.out.print(" "+pq.poll());
		}

		PriorityQueue<Employee> pQueue = new PriorityQueue<Employee>(new EmployeeComparator());
		pQueue.addAll(Arrays.asList(arrayOfEmps));
		System.out.println("Printing Employee Priority Queue: ");
		while (!pQueue.isEmpty()) {
            System.out.print("=> "+pQueue.poll());
		}

		// Similar to above we can do: 
		// public class Employee implements Comparable<Employee> {
		//
		//	@Override
	    //	public int compareTo(Employee o) {
	    //  	  return e1.getName().compareTo(e2.getName());
	    //	}
		//}

		System.out.println();
		
		//////////////////////////////////////////
		// BlockingQueue
		//////////////////////////////////////////
		
		System.out.println("\n==> BlockingQueue");
		
		// Below is bounded queue. We can create such queues by passing the capacity as an argument to a constructor
		BlockingQueue<Integer> blockingQueue = new LinkedBlockingDeque<>(10);

		// Creating unbounded queues is simple:
		blockingQueue = new LinkedBlockingDeque<>();
		blockingQueue.addAll(listOfNums);
		
		System.out.println("Printing elements of Blocking queue: ");
		while(!blockingQueue.isEmpty()) {
			System.out.print(" "+blockingQueue.poll());
		}
		
		System.out.println();
		// ========================================
		//	===================================  Set
		// ========================================
		System.out.println("\n==> Set");
		Set<Integer> setOfInts = new HashSet<Integer>();
		setOfInts.addAll(listOfNums);

		System.out.println("Printing elements of Set: "+setOfInts);

		Set<Integer> a = new HashSet<Integer>();
        // Adding all elements to List
        a.addAll(Arrays.asList(new Integer[] { 1, 3, 2, 4, 8, 9, 0 }));
       
        // Again declaring object of Set class
        // with reference to HashSet
        Set<Integer> b = new HashSet<Integer>();
        b.addAll(Arrays.asList(new Integer[] { 1, 3, 7, 5, 4, 0, 7, 5 }));
     
        System.out.println("\nSet a: "+a);
        System.out.println("Set b: "+b);
		// To find union
        Set<Integer> union = new HashSet<Integer>(a);
        union.addAll(b);
        System.out.print("\nUnion of the two using Set.addAll(): "+union);
 
        // To find intersection
        Set<Integer> intersection = new HashSet<Integer>(a);
        intersection.retainAll(b);
        System.out.print("\nIntersection of the two using Set.retainAll() : "+ intersection);
 
        // To find the symmetric difference
        Set<Integer> difference = new HashSet<Integer>(a);
        difference.removeAll(b);
        System.out.print("\nDifference of the two using Set.removeAll(): "+difference);
        
        // ==========> EnumSet:
        // It is a high-performance set implementation, much faster than HashSet. 
        // All of the elements in an enum set must come from a single enumeration type that is specified when 
        // the set is created either explicitly or implicitly.
        Set<Gfg> set1 = EnumSet.of(Gfg.QUIZ, Gfg.CONTRIBUTE, Gfg.LEARN, Gfg.CODE);
        System.out.println("\nSet from enum: " + set1);
        
        // ==========> LinkedHashSet:
        // LinkedHashSet class which is implemented in the collections framework is an ordered version of HashSet that maintains a 
        //	doubly-linked List across all elements.
        Set<Integer> lhSet = new LinkedHashSet<Integer>();
        lhSet.addAll(listOfNums);
        System.out.println("Printing elements of LinkedHashSet: "+lhSet);
        
        // ==========> TreeSet:
        // It behaves like a simple set with the exception that it stores elements in a sorted format. 
        //	TreeSet uses a tree data structure for storage. Objects are stored in sorted, ascending order. 
        //	But we can iterate in descending order using the method TreeSet.descendingIterator()
        
        // TreeSet ts = new TreeSet(Comparator comp);
        
        Set<Integer> treeSet = new TreeSet<Integer>();
        treeSet.addAll(listOfNums);
        System.out.println("Printing elements of TreeSet: "+treeSet);
        
        
        System.out.println("\n");
		// ====================
		//	Map / HashMap
		// ====================
        // - A Map cannot contain duplicate keys and each key can map to at most one value. 
        //		Some implementations allow null key and null values like the HashMap and LinkedHashMap, but some do not like the TreeMap.
        // - The order of a map depends on the specific implementations. 
        //		For example, TreeMap and LinkedHashMap have predictable orders, while HashMap does not.
        // - There are two interfaces for implementing Map in java. They are Map and SortedMap, and three 
        //		classes: HashMap, TreeMap, and LinkedHashMap.

		Map<String, String> map = new HashMap<String, String>(40);

		TreeMap<Employee, Double> tMap = new TreeMap<Employee, Double>(new EmployeeComparator());
		Arrays.asList(arrayOfEmps).forEach(emp -> tMap.put(emp, emp.getSalary()));

		for(Map.Entry<Employee, Double> entry: tMap.entrySet()) {
			System.out.println(entry.getKey().getName() +" <-> "+entry.getValue());
		}

		System.out.println("\n");
	}
}
