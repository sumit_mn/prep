package java8;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import models.Employee;
import util.EmployeeRepository;

/**
 * Java 8 Streams are really nothing more than a way of providing functional programming techniques to Java collections. 
 * 
 * @author Sumeet Mahajan
 *
 */
public class Streams {

	static Employee[] arrayOfEmps = {
			new Employee(1, "Jeff Bezos", 100000.0), 
			new Employee(2, "Bill Gates", 200000.0), 
			new Employee(3, "Mark Zuckerberg", 300000.0)
	};

	static EmployeeRepository employeeRepository = new EmployeeRepository(Arrays.asList(arrayOfEmps));

	public static void main(String[] args) {
		streamCreation();

		streamOperations();
	}

	/**
	 * Java Stream creation
	 */
	private static void streamCreation(){
		// ====================
		//	1. Using Stream.of()
		// ====================
		@SuppressWarnings("unused")
		Stream<Employee> empStream = Stream.of( arrayOfEmps );
		empStream = Stream.of(arrayOfEmps[0], arrayOfEmps[1], arrayOfEmps[2]);
		
		// ============================================================
		//	2. Java 8 added a new stream() method to the Collection interface -> Collection.stream()
		// ============================================================
		List<Employee> empList = Arrays.asList(arrayOfEmps);
		empStream = empList.stream();

		// ====================
		//	3. Using StreamBuilder
		// ====================
		Stream.Builder<Employee> empStreamBuilder = Stream.builder();

		empStreamBuilder.accept(arrayOfEmps[0]);
		empStreamBuilder.accept(arrayOfEmps[1]);
		empStreamBuilder.accept(arrayOfEmps[2]);

		empStream = empStreamBuilder.build();
	}
	
	/**
	 * Java Stream Operations
	 * 
	 * Computation on the source data is only performed when the terminal operation is initiated, and source elements are consumed 
	 * only as needed. All intermediate operations are lazy, so they�re not executed until a result of a processing is actually needed.
	 * 
	 */
	@SuppressWarnings("unused")
	private static void streamOperations() {
		List<Employee> empList = Arrays.asList(arrayOfEmps);

		// forEach() 
		// is a terminal operation, which means that, after the operation is performed, 
		// the stream pipeline is considered consumed, and can no longer be used.
		empList.stream().forEach(e -> e.salaryIncrement(10.0));

		// map() 
		// produces a new stream after applying a function to each element of the original stream. 
		// The new stream could be of different type.
		Integer[] empIds = { 1, 2, 3 };

	    List<Employee> employees = Stream.of(empIds)
	      .map(employeeRepository::findById)
	      .collect(Collectors.toList());

		System.out.println("Number of employees: "+employees.size());

		// collect() 
		// works in the previous example; its one of the common ways to get stuff out of the stream 
		// once we are done with all the processing:
		employees = empList.stream().collect(Collectors.toList());

		// filter() 
		// produces a new stream that contains elements of the original stream that pass a given test (specified by a Predicate)
		employees = Stream.of(empIds)
			      .map(employeeRepository::findById)
			      .filter(e -> e != null)
			      .filter(e -> e.getSalary() > 200000)
			      .collect(Collectors.toList());
		
		// findFirst() 
		// returns an Optional for the first entry in the stream; the Optional can, of course, be empty:
		Employee employee = Stream.of(empIds)
			.map(employeeRepository::findById)
			.filter(e -> e != null)
			.filter(e -> e.getSalary() > 100000)
			.findFirst()
			.orElse(null);

		// toArray()
		// We saw how we used collect() to get data out of the stream. 
		// If we need to get an array out of the stream, we can simply use toArray():
		Employee[] employeesArr = empList.stream().toArray(Employee[]::new);

		// flatMap()
		// A stream can hold complex data structures like Stream<List<String>>. In cases like this, flatMap() 
		// helps us to flatten the data structure to simplify further operations:

		List<List<String>> namesNested = Arrays.asList( 
		      Arrays.asList("Jeff", "Bezos"), 
		      Arrays.asList("Bill", "Gates"), 
		      Arrays.asList("Mark", "Zuckerberg"));

		List<String> singleList = namesNested.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList());

		// peek() - can be useful in situations like this. 
		// Simply put, it performs the specified operation on each element of the stream and returns a new stream which can be used further. 
		// peek() is an intermediate operation
		 empList.stream()
	      .peek(e -> e.salaryIncrement(10.0))
	      .peek(System.out::println)
	      .collect(Collectors.toList());
		 
		 // ===================================
		 // Comparison
		 // ===================================

		 // sorted()
		 employees = empList.stream()
			      .sorted((e1, e2) -> e1.getName().compareTo(e2.getName()))
			      .collect(Collectors.toList());

		 // min() and max()
		 // based on a comparator. They return an Optional since a result may or may not exist (due to, say, filtering)
		 Employee firstEmp = empList.stream()
			      .min((e1, e2) -> e1.getId() - e2.getId())
			      .orElseThrow(NoSuchElementException::new);

		 Employee maxSalEmp = empList.stream()
			      .max(Comparator.comparing(Employee::getSalary))
			      .orElseThrow(NoSuchElementException::new);
		 
		 // distinct()
		 // it does not take any argument and returns the distinct elements in the stream, eliminating duplicates. 
		 // It uses the equals() method of the elements to decide whether two elements are equal or not
		 List<Integer> intList = Arrays.asList(2, 5, 3, 2, 4, 3);
		 List<Integer> distinctIntList = intList.stream().distinct().collect(Collectors.toList());
		 
		 // allMatch(), anyMatch(), and noneMatch()
		 // These operations all take a predicate and return a boolean. 
		 // Short-circuiting is applied and processing is stopped as soon as the answer is determined:
		 // allMatch() checks if the predicate is true for all the elements in the stream. 
		 // 	Here, it returns false as soon as it encounters 5, which is not divisible by 2.
		 // anyMatch() checks if the predicate is true for any one element in the stream. 
		 //		Here, again short-circuiting is applied and true is returned immediately after the first element.
		 // noneMatch() checks if there are no elements matching the predicate. 
		 //		Here, it simply returns false as soon as it encounters 6, which is divisible by 3.
		 intList = Arrays.asList(2, 4, 5, 6, 8);
		    
		 boolean allEven = intList.stream().allMatch(i -> i % 2 == 0);
		 boolean oneEven = intList.stream().anyMatch(i -> i % 2 == 0);
		 boolean noneMultipleOfThree = intList.stream().noneMatch(i -> i % 3 == 0);

		 // ==============================================================
		 // Stream Specializations - IntStream, DoubleStream, LongStream
		 // ==============================================================

		 // From what we discussed so far, Stream is a stream of object references. However, there are also the 
		 //	IntStream, LongStream, and DoubleStream � which are primitive specializations for int, long and double respectively. 
		 //	These are quite convenient when dealing with a lot of numerical primitives.
		 // 	These specialized streams do not extend Stream but extend BaseStream on top of which Stream is also built.
		 //		As a consequence, not all operations supported by Stream are present in these stream implementations. 
		 //	For example, the standard min() and max() take a comparator, whereas the specialized streams do not.

		 // The most common way of creating an IntStream is to call mapToInt() on an existing stream:
		 // 1. using mapToInt()
		 empList.stream()
		 	.mapToInt(Employee::getId)
		 	.max()
		 	.orElseThrow(NoSuchElementException::new);

		 // 2. using of()
		 IntStream.of(1, 2, 3);

		 // 3. using range() as below which creates IntStream of numbers 10 to 19.
		 IntStream.range(10, 20).forEach(System.out::println);

		 // Specialized Operations
		 // Specialized streams provide additional operations as compared to the standard Stream � which are quite convenient when dealing with numbers.
		 // For example sum(), average(), range() etc:

		 Double avgSal = empList.stream()
		      .mapToDouble(Employee::getSalary)
		      .average()
		      .orElseThrow(NoSuchElementException::new);

		 // ======================================================================
		 // Reduce operations
		 // ======================================================================
		 // A reduction operation (also called as fold) takes a sequence of input elements and combines them into a 
		 // single summary result by repeated application of a combining operation. 
		 // We already saw few reduction operations like findFirst(), min() and max().

		 Double sumSal = empList.stream()
			      .map(Employee::getSalary)
			      .reduce(0.0, Double::sum);

		 // =========================================================================================================
		 // Advanced collect / Collectors - joining(), toSet(), summarizingDouble(), partitioningBy(), groupingBy(), mapping(), reducing()
		 // =========================================================================================================

		 // joining()
		 // It internally uses a java.util.StringJoiner to perform the joining operation
		 empList.stream()
	      .map(Employee::getName)
	      .collect(Collectors.joining(", "))
	      .toString();

		 // toSet()
		 Set<String> empNames = empList.stream()
		            .map(Employee::getName)
		            .collect(Collectors.toSet());

		 // summarizingDouble()
		 DoubleSummaryStatistics stats = empList.stream()
			      .collect(Collectors.summarizingDouble(Employee::getSalary));

		 System.out.println("count: "+ stats.getCount());
		 System.out.println("sum: "+ stats.getSum());
		 System.out.println("min: "+stats.getMin());
		 System.out.println("max: "+stats.getMax());
		 System.out.println("average: "+stats.getAverage());
		 
		 // partitioningBy()
		 // We can partition a stream into two � based on whether the elements satisfy certain criteria or not.
		 // Let�s split our List of numerical data, into even and odds:
		 // Here, the stream is partitioned into a Map, with even and odds stored as true and false keys.
		 intList = Arrays.asList(2, 4, 5, 6, 8);
		 Map<Boolean, List<Integer>> isEven = intList.stream().collect(
				Collectors.partitioningBy(i -> i % 2 == 0));

		 System.out.println("Even elements count: "+isEven.get(true).size());
		 System.out.println("Odd elements count: "+isEven.get(false).size());

		 // groupingBy()
		 // offers advanced partitioning � where we can partition the stream into more than just two groups.
		 // It takes a classification function as its parameter. This classification function is applied to each element of the stream.
		 // The value returned by the function is used as a key to the map that we get from the groupingBy collector:
		 Map<Character, List<Employee>> groupByAlphabet = empList.stream().collect(
			      Collectors.groupingBy(e -> new Character(e.getName().charAt(0))));

		 System.out.println("Element starting with B: "+ groupByAlphabet.get('B').get(0).getName());
		 System.out.println("Element starting with M: "+ groupByAlphabet.get('M').get(0).getName());
		 
		 // mapping()
		 // groupingBy() discussed in the section above, groups elements of the stream with the use of a Map.
		 // However, sometimes we might need to group data into a type other than the element type.
		 // Here�s how we can do that; we can use mapping() which can actually adapt the collector to a different type � using a mapping function:
		 Map<Character, List<Integer>> idGroupedByAlphabet = empList.stream().collect(
			      Collectors.groupingBy(e -> new Character(e.getName().charAt(0)),
			        Collectors.mapping(Employee::getId, Collectors.toList())));
		 
		 // Here mapping() maps the stream element Employee into just the employee id � which is an Integer � using the getId() mapping function. 
		 // These ids are still grouped based on the initial character of employee first name.
		 System.out.println("ID of element starting with B: "+ idGroupedByAlphabet.get('B').get(0));
		 System.out.println("ID of element starting with J: "+ idGroupedByAlphabet.get('J').get(0));
		 
		 // reducing()
		 // is similar to reduce() � which we explored before. It simply returns a collector which performs a reduction of its input elements:
		 Double percentage = 10.0;
		 Double salIncrOverhead = empList.stream().collect(Collectors.reducing(
			        0.0, e -> e.getSalary() * percentage / 100, (s1, s2) -> s1 + s2));
		 // Here reducing() gets the salary increment of each employee and returns the sum.
		 System.out.println("salIncrOverhead = "+ salIncrOverhead);
		 
		 // reducing() is most useful when used in a multi-level reduction, downstream of groupingBy() or partitioningBy(). 
		 // To perform a simple reduction on a stream, use reduce() instead.
		 Comparator<Employee> byNameLength = Comparator.comparing(Employee::getName);
		 
		 Map<Character, Optional<Employee>> longestNameByAlphabet = empList.stream().collect(
			      Collectors.groupingBy(e -> new Character(e.getName().charAt(0)),
			        Collectors.reducing(BinaryOperator.maxBy(byNameLength))));
		 
		 // Here we group the employees based on the initial character of their first name. 
		 // Within each group, we find the employee with the longest name.
		 System.out.println("Longest employee name starting with B"+ longestNameByAlphabet.get('B').get().getName());
		 System.out.println("Longest employee name starting with J"+ longestNameByAlphabet.get('J').get().getName());
		 
		 // ==================
		 // Parallel Streams
		 // ==================
		 
		 // Using the support for parallel streams, we can perform stream operations in parallel without 
		 // having to write any boilerplate code; we just have to designate the stream as parallel
		 empList.stream().parallel().forEach(e -> e.salaryIncrement(10.0));
		 
		 // 1. We need to ensure that the code is thread-safe. Special care needs to be taken 
		 // 	if the operations performed in parallel modifies shared data.
		 //	2. We should not use parallel streams if the order in which operations are performed or the 
		 // 	order returned in the output stream matters. For example operations like findFirst() may generate the 
		 //		different result in case of parallel streams.
		 // 3. Also, we should ensure that it is worth making the code execute in parallel. Understanding the performance 
		 //		characteristics of the operation in particular, but also of the system as a whole � is naturally very important here.
		 
		 // ==================
		 // Infinite Streams
		 // ==================
		 // generate() 
		 // We provide a Supplier to generate() which gets called whenever new stream elements need to be generated:
		 Stream.generate(Math::random)
	      .limit(5)
	      .forEach(System.out::println);

		 // With infinite streams, we need to provide a condition to eventually terminate the processing. 
		 // One common way of doing this is using limit(). In above example, we limit the stream to 5 random 
		 // numbers and print them as they get generated.
		 // Please note that the Supplier passed to generate() could be stateful and such stream may not produce the same result when used in parallel.

		 // iterate() 
		 // takes two parameters: an initial value, called seed element and a function which generates next element using the previous value. iterate(), 
		 // by design, is stateful and hence may not be useful in parallel streams:
		 Stream<Integer> evenNumStream = Stream.iterate(2, i -> i * 2);

		 List<Integer> collect = evenNumStream
				 .limit(5)
				 .collect(Collectors.toList());
		 System.out.println("List using iterate: "+collect.toString());
		 // Here, we pass 2 as the seed value, which becomes the first element of our stream. This value is passed as 
		 // 	input to the lambda, which returns 4. This value, in turn, is passed as input in the next iteration.
		 // This continues until we generate the number of elements specified by limit() which acts as the terminating condition.
		 
	}
}
