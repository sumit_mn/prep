package java8;

import java.util.Arrays;
import java.util.List;

public class DeclarativePrgramming {

	/**
	 * - Functional programming is a paradigm that allows programming using expressions i.e. declaring functions, passing 
	 * 	 	functions as arguments and using functions as statements. 
	 * - The imperative approach defines a program as a sequence of statements that change the program's state until it reaches the final state. 
	 * 	 	Procedural programming is a type of imperative programming where we construct programs using procedures or subroutines. 
	 * 	 	One of the popular programming paradigms known as object-oriented programming (OOP) extends procedural programming concepts. 
	 * - In contrast, the declarative approach expresses the logic of a computation without describing its control flow in terms of a 
	 * 	 	sequence of statements. Simply put, the declarative approach's focus is to define what the program has to achieve rather than how 
	 * 	 	it should achieve it. Functional programming is a sub-set of the declarative programming languages.
	 * - Pure functions - The definition emphasizes that a pure function should return a value based only on the arguments and should have no side effects.
	 * - Side effects can be anything apart from the intended behavior of the method. For instance, side-effects can be as simple as updating a local 
	 * 	 	or global state or saving to a database before returning a value. Purists also treat logging as a side effect,
	 * - Immutability is one of the core principles of functional programming, and it refers to the property that an entity can't be 
	 * 		modified after being instantiated.
	 * - Please note that Java itself provides several built-in immutable types, for instance, String. This is primarily for security reasons, 
	 * 		as we heavily use String in class loading and as keys in hash-based data structures. There are several other built-in immutable types like primitive wrappers and math types.
	 * - The biggest advantage of adopting functional programming in any language, including Java, is pure functions and immutable states. 
	 * 		If we think in retrospect, most of the programming challenges are rooted in the side-effects and mutable state one way or the other. 
	 * 		Simply getting rid of them makes our program easier to read, reason about, test, and maintain.
	 * 
	 * https://www.baeldung.com/java-functional-programming
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(11, 22, 33, 44, 55, 66, 77, 88, 99, 100);

		// Imperative Programming: 
		int result = 0;
		for (Integer n : numbers) {
			if (n % 2 == 0) {
				result += n * 2;
			}
		}
		System.out.println(result);

		// The first issue with the above code is that we are mutating the variable result again and again. 
		//	 So mutability is one of the biggest issues in an imperative style of coding. 
		//	 The second issue with the imperative style is that we spend our effort telling not only what to do 
		//	 but also how to do the processing. Now let�s re-write above code in a declarative style.

		// Declarative programming
		result = numbers.stream()
					.filter(n -> n % 2 == 0)
					.mapToInt(n -> n * 2)
					.sum();

		System.out.println(result);
		
		
		int factor = 2;
        System.out.println(
            numbers.stream()
                .filter(number -> number % 2 == 0)
                .mapToInt(e -> e * factor)
                .sum());
        //factor=3; // causes error
	}
}