package java8;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Java 8 introduced CompletableFuture with a combination of a Future and CompletionStage. 
 * It provides various methods like supplyAsync, runAsync, and thenApplyAsync for asynchronous programming.
 * 
 * @author sumeet
 *
 */
public class AsyncInJava8 {
	
	static int factorial(int n) {
        if (n == 0 || n == 1)
            return 1;
        else
            return n * factorial(n - 1);
    }
	
	/**
	 * The CompletableFuture internally uses ForkJoinPool to handle the task asynchronously. Thus, it makes our code a lot cleaner.
	 * 
	 * In CompletableFuture there is a callback named complete(). We can call the complete method manually to complete that task.
	 * 
	 * @param args
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		int number  = 10011;
		CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> factorial(number));
		while (!completableFuture.isDone()) {
		    System.out.println("CompletableFuture is not finished yet...");
		}
		System.out.println("result: "+completableFuture.get());
		
	}
}
