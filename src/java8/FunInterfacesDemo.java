package java8;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

import impl.FunctionalAddThreeImpl;
import impl.FunctionalPredicateImpl;
import interfaces.FunctionalSum;


/**
 * 
 * @FunctionalInterface
 * 
 * A functional interface is also called as SAM interface or Single Abstract Method interface. 
 * A functional interface allows exactly one �abstract method� as its member.
 * 
 * By definition, default methods are Non-abstract and you can add as many default methods in the functional interface as you like.
 *
 */
public class FunInterfacesDemo {

	public static void main(String[] args) {

		FunctionalSum fSum = (int x, int y) -> System.out.println(x + y);

		System.out.println("The result is: ");
		fSum.abstractSum(10, 5);
		fSum.defaultFun();
		fSum.defaultFun2();

		// =====================================================================
		//  The Function functional interface represents a function (method) that takes a 
		//  single parameter and returns a single value
		// =====================================================================
		FunctionalAddThreeImpl adder = new FunctionalAddThreeImpl();
		Long result = adder.apply(4l);
		System.out.println("Result of 4 + 3 = "+ result);

		Function<Long, String> adderFun = (x) -> x + "3";
		String res = adderFun.apply(5l);
		System.out.println("String 5 + 3 = "+ res);

		System.out.println("");
		// =====================================================================
		//  The Java Predicate interface, java.util.function.Predicate, 
		//  represents a simple function that takes a single value as parameter, and returns true or false.
		// =====================================================================

		FunctionalPredicateImpl pDemo = new FunctionalPredicateImpl();
		boolean pRes = pDemo.test("3");
		System.out.println("predicate result: "+pRes);

		Predicate<String> p = (x) -> x.equals("3");
		pRes = p.test("2");
		System.out.println("predicate result: "+pRes);
		
		System.out.println("");
		// UnaryOperator =====================================================================
		// The Java UnaryOperator interface is a functional interface that represents 
		// an operation which takes a single parameter and returns a parameter of the same type
		//=====================================================================
		UnaryOperator<String> currencyFormatter = (x) -> "$"+x;
		System.out.println("Amout to Currency: "+currencyFormatter.apply("1000"));
		
		// Supplier =====================================================================
		// The Java Supplier interface is a functional interface that represents an function that 
		// supplies a value of some sorts. The Supplier interface can also be thought of as a factory interface.
		// Here is an example implementation of the Java Supplier interface:
		//=====================================================================

		Supplier<Integer> supplier = () -> new Integer((int) (Math.random() * 1000D));
		System.out.println("Supplied Integer = "+supplier.get());
		//This Java Supplier implementation returns a new Integer instance with a random value between 0 and 1000.

		// Consumer =====================================================================
		// The Java Consumer interface is a functional interface that represents an function that consumes a value without returning any value. 
		// A Java Consumer implementation could be printing out a value, or writing it to a file, 
		// or over the network etc. Here is an example implementation of the Java Consumer interface:
		//=====================================================================
		@SuppressWarnings("unused")
		Consumer<Integer> consumer = (value) -> System.out.println(value);
		System.out.println("");
		//=====================================================================
		// Function Composition
		//=====================================================================

		// compose() =====================================================================
		// The Function returned by compose() will first call the Function passed as parameter to compose(), 
		// and then it will call the Function which compose() was called on.
		//=====================================================================
		
		Function<Integer, Integer> multiply = (value) -> value * 2;
		Function<Integer, Integer> add      = (value) -> value + 3;

		Function<Integer, Integer> addThenMultiply = multiply.compose(add);

		Integer result1 = addThenMultiply.apply(3);
		System.out.println("compose() returns: "+result1);
		System.out.println("");
		
		// addThen() =====================================================================
		// The Java Function andThen() method works opposite of the compose() method. 
		// A Function composed with andThen() will first call the Function that andThen() was called on, 
		// and then it will call the Function passed as parameter to the andThen() method.
		// =====================================================================

		Function<Integer, Integer> fMmultiply = (value) -> value * 2;
		Function<Integer, Integer> fAdd      = (value) -> value + 3;

		Function<Integer, Integer> multiplyThenAdd = fMmultiply.andThen(fAdd);

		Integer result2 = multiplyThenAdd.apply(3);
		System.out.println("andThen() returns: "+result2);
		
	}
}
