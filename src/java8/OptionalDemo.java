package java8;

import java.util.Optional;

import models.Address;
import models.User;

public class OptionalDemo {

	public static void main(String[] args) {
		Optional<User> user = Optional.ofNullable(getUser());
		String result = user
		  .map(User::getAddress)
		  .map(Address::getStreet)
		  .orElse("not specified");
		
		System.out.println("result is: "+result);
	}

	static User getUser(){
		User u = new User();
		u.setAddress(new Address());
		//u.getAddress().setStreet("Testblvd");

		return u;
	}

	@SuppressWarnings("unused")
	private static String oldStyle() {
		User user = getUser();
		if (user != null) {
		    Address address = user.getAddress();
		    if (address != null) {
		        String street = address.getStreet();
		        if (street != null) {
		            return street;
		        }
		    }
		}
		return "not specified";
	}
}
